FROM node:10 as builder
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
ENV NODE_OPTIONS="--max_old_space_size=2048"
COPY ./files/webpack/webpack.config.js /app/node_modules/oskari-frontend/webpack.config.js
RUN npm run build

FROM nginx
COPY --from=builder /app/dist /app/dist
EXPOSE 80
COPY ./files/nginx /etc/nginx
